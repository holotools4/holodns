#!/usr/bin/perl

my $file = shift || 'DNSrecord.csv';
my $dotf = $file; $dotf =~ s/.csv/.dot/;
my $table = &loadCSV($file);

local *D; 
open D,'>', $dotf;
%seen = ();

print  D "digraph record {\n";
print  D <<"EOT";
graph [pad="1.8", nodesep="2", ranksep="3"];
    //ratio="auto";
    splines="true";
    node[pad=20];
    //edge[style=invis];
EOT

foreach $r (@{$table}) {
   printf "r: %s\n",join',',@{$r};
   if ($r->[3] =~ m/^"(.*)"$/) {
      $r->[3] = $1; 
   }
   # A record ...
   if ( $r->[0] eq '' ) {
     printf D qq' "_blank" -> "?%s"\n', $r->[3] unless $seen{'?'.$r->[3]}++;
     next;
   } elsif ( $r->[0] =~ m/^#/) {
     #y $s = join', ',@{$r}; $s =~ s/"/'/g;
     if ($r->[3]) {
       printf D qq' "comment" -> "# %s"\n', $r->[3] unless $seen{'#'.$r->[3]}++;
     } 
     next;
   } elsif ( $r->[2] eq 'A') {
      if ($r->[1] eq '@') {
	 printf D qq' "%s" [ label="%s",shape="box" ]\n',$r->[0],$r->[0];
	 printf D qq' "%s" -> "%s"\n', $r->[0],$r->[3] if $r->[3];
      } elsif ($r->[1] eq 'www') {
	 printf D qq' "%s.%s" -> "%s"\n', $r->[1],$r->[0],$r->[3] if $r->[3];
      } elsif ($r->[1] =~ m/^[a-z]/i) {
	 printf D qq'"%s.%s" -> "%s"\n',$r->[1],$r->[0], $r->[3] if $r->[3];
      }
   } elsif ($r->[2] eq 'CNAME') {
      printf D qq' "%s.%s" -> "%s"\n',$r->[1],$r->[0], $r->[3] if $r->[3];
   # counts ...
   } elsif ( $r->[2] eq 'TXT' && $r->[3] =~ m/counts/) {
      $a = $r->[0]; $a =~ s/\./\@/ unless ($a =~ m/\@/); $a =~ s/\.$//;
      $c = $1 if ($r->[3] =~ m/counts=(\d+)/);
      printf D qq' "C%s" [ label="%s" ]\n',$c,$r->[3] unless $seen{'C'.$c}++;
      printf D qq' "freenom/counts" -> "%s" -> "C%s"\n',$a,$c;
   } elsif ( $r->[1] eq 'pending' && $r->[2] eq 'TXT') {
     # nop
   # contact RP
   } elsif ( $r->[2] eq 'RP' ) {
     my ($a,$n) = split' ',$r->[3];
     $a =~ s/\./\@/ unless ($a =~ m/\@/); $a =~ s/\.$//;
     $n =~ s/\.$//;
     printf D qq'"%s" -> "%s"\n',$a,$n unless ($seen{'RP'.$a} || $n eq '.' || $n eq '');
     printf D qq'"%s" -> "contact.%s"\n',$a,$r->[0] if ($r->[0] ne '');
     printf D qq'// "%s" -> "contact.%s"\n',$r->[0],$r->[0];
     printf D qq'// "%s" -> contact\n',$a unless $seen{'RP'.$a}++;
   # expiration dates ....
   } elsif ( $r->[1] eq '_expires' && $r->[2] eq 'TXT') {
      $r->[3] =~ s/;.*//;
      my ($y,$m,$d);
      if ($r->[3] =~ m,/,) {
	 ($m,$d,$y) = split /\//,$r->[3];
	 printf D qq'"expiration -> %s" -> "USY%s"\n',$y,$y unless $seen{'USY'.$y}++;
	 printf D qq'"USY%s" -> "%s/%s" -> "%s/%s"\n',$y,$y,$m,$m,$d unless $seen{$r->[3]}++;
	 printf D qq'"%s/%s"-> "_expires.%s"\n',$m,$d,$r->[0];
      } elsif ($r->[3] =~ m,-,) {
         ($y,$m,$d) = split /\-/,$r->[3];
	 printf D qq'"expiration -> %s" -> "EUY%s"\n',$y,$y unless $seen{'EUY'.$y}++;
         printf D qq'"EUY%s" -> "%s-%s" -> "%s-%s"\n',$y,$y,$m,$m,$d unless $seen{$r->[3]}++;
         printf D qq'"%s-%s"-> "_expires.%s"\n',$m,$d,$r->[0];
      } elsif ($r->[3] =~ m,\.,) {
         ($d,$m,$y) = split /\./,$r->[3];
	 printf D qq'expiration -> "%s" -> "CHY%s"\n',$y,$y unless $seen{'CHY'.$y}++;
         printf D qq'"CHY%s" -> "%s.%s" -> "%s.%s"\n',$y,$y,$m,$m,$d unless $seen{$r->[3]}++;
         printf D qq'"%s.%s" -> "_expires.%s"\n',$m,$d,$r->[0];
      }
      printf D qq'"%s" [ shape="box", type="filled", color="red" ]\n',$y unless $seen{'YY'.$y};
   # DNS ...
   } elsif ($r->[2] eq 'NS') {
      if ($r->[3] =~ m/(netlify|helio|freenom)/) {
	 my ($ns,$dns) = split'\.',$r->[3],2;
	 #printf D qq'"DNS" -> "%s"\n',$dns unless $seen{$dns}++;
	 printf D qq'"%s" -> DNS\n',$dns unless $seen{$dns}++;
	 printf D qq' "%s" -> "%s"\n',$dns,$r->[3] unless $seen{'NS'.$r->[3]}++;
	 printf D qq'"%s" -> "*.%s"\n',$dns,$r->[0];
      }

   }
   if ($r->[2] eq 'CNAME') {
      if ($r->[3] =~ m/(netlify|cloudflare|ipfs)/ && ! $seen{$r->[3]}++) {
	 my $h = ($r->[1] ne '@') ? $r->[1].'.'.$r->[0] : $r->[0];
	 printf D qq' "%s" -> "%s"\n',$1,$r->[3];
	 printf D qq' "CNAME" -> "%s"\n',$1 unless $seen{'CN'.$1}++;
      }
   } elsif ($r->[2] eq 'A' && $r->[3] ne '')  {
      printf D qq' "%s" -> "%s"\n',$r->[2],$r->[3] unless $seen{'A'.$r->[3]}++;
   }

}
printf D " }\n";
close D;

#use YAML::Syck qw(Dump); printf "t: %s.\n",Dump($table);
exit $?;

sub loadCSV {
 my $f = shift;
 local *CSV; open CSV,'<',$f; local $/ = undef;
 my $buf = <CSV>; close CSV;
 my $t = [];
 foreach my $line (split "\n",$buf) {
  if ($line =~ m/"[^"]*,[^"]*"/) {
   # protect commas *nested substitution*
   $line =~ s/"(.*?[^"])"/($m=$1)=~(s#,#%2C#g);$m/ge; # urlencode(',') = '%2C'; # _comma_
  
  push @{$t}, [ map { s/%2C/,/g; $_; } split/,\s*/,$line ];
 }
 return $t;
}


1; # $Source: /my/perl/scripts/csv2dot.pl$
