---
---
# holoDNS

This presents the tools to make our knowledge and services discoverable on the internet !

However we stay away from a centralized approache to DNS and opt for a
 distributed DNS/LNS
 
 _more to read soon_ ...


### DNS server for our websites

 - <https://freenom.com/> [accounts](http://localhost:8088/usb/repos/gitlab.com/holoVerse/holoTools/holoDNS/accounts.yml)
 - [Tommy's cPanel](https://tommy.heliohost.org:2083/cpsess6430122666/frontend/paper_lantern/index.html)
 - IP address [myip](https://www.myip.com/)
   - <https://whatismyipaddress.com/> (wimi)
   - <https://dynsm.ml/cgi-bin/remote_addr.pl>
   - <https://www.cloudflare.com/cdn-cgi/trace>
   - <https://postman-echo.com/ip?format=text>
 
## Links

* [README](README.md)
* Records: [DNSrecords](DNSrecords.csv)
* mydomain: <https://holotools4.frama.io/holodns/mydomain.htm>
* expiration: <https://framagit.org/michelsphere/mywiki/-/wikis/expiration>
* [punyhash](punyhash) 
* punycode: <http://punycoder.com>
* unicode: <https://unicode-table.com/en/>

