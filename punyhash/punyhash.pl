#!/usr/bin/env perl

use Encode;
use Encode::Punycode;

use lib $ENV{SITE}.'/lib';
use UTIL qw(decode_base32 decode_base58 encode_base32 encode_base36 encode_base39 encode_base58 encode_basen encode_basea);

use open qw(:std :encoding(utf8));

#binmode(STDOUT,':utf8');

my $qm = shift || 'QmeXS5cMyuTVU63codTi83sNRRPhvZSmw3ASk3ESebcsLk';

my $mhash;
if ($qm =~ m/^f/) {
   $mhash = pack'H*',substr($qm,1);
} elsif ($qm =~ m/^z/) {
   # base58 : no l,I,O,0
   $qm =~ tr/0-9a-zA-Z/o1-9a-kLm-zA-HiJ-NoP-Z/; # change confusing char.
   $mhash = &decode_base58(substr($qm,1));
} elsif ($qm =~ m/^b/) {
   $mhash = &decode_base32(substr($qm,1));
} elsif ($qm =~ m/^k/) {
   $mhash = &decode_base36(substr($qm,1));
} elsif ($qm =~ m/^Qm/) {
   $qm =~ tr/0-9a-zA-Z/o1-9a-kLm-zA-HiJ-NoP-Z/; # change confusing char.
   $mhash = &decode_base58($qm);
}

my $mh32 = 'b'. &encode_base32($mhash);
my $mh36 = 'k'. &encode_base36($mhash);
my $mh38 = '?'. &encode_basen($mhash,38); $mh38 =~ y/./_/;
my $mh39 = '?'. &encode_base39($mhash);
my $mh58 = 'z'. &encode_base58($mhash);

printf "hf16: %s (%uc)\n",unpack('H*',$mhash),2*length($mhash);
printf "hb32: %s (%uc)\n",$mh32,length($mh32);
printf "hk36: %s (%uc)\n",$mh36,length($mh36);
printf "h*38: %s (%uc)\n",$mh38,length($mh38);
printf "hx39: %s (%uc)\n",$mh39,length($mh39);
printf "hz58: %s (%uc)\n",$mh58,length($mh58);
my $url38 = 'url63'.decode_utf8('я').substr($mh38,1); $url38 =~ s/([^\.]{63})/\1./g;
printf "url: http://www.%s.localhost:8088/\n",$url38;

# base /
# len * l(256)/l(base) = 63
# len * l(256) = 63 * l(base)
# lbase = len * l(256) / 63
my $radix = exp(length($mhash) * log(256) / 63 );
printf "radix: %f\n",$radix;
if ($mh58 =~ /[A-Z]/) {
my $GR = decode_utf8 'ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ'; # 24
my $FR = decode_utf8 'áéíóú'.'àèìòù'.'âêîôû'.'äëïöüÿ'.'çœæ'; # 24
my $AB = substr(decode_utf8('αβγδελμνπρστΦχω'.'ï'.'éèêë'.'àá'.'ç'.'œ'),0,24);
printf "AB:  %s (%uc)\n",$AB,length($AB);
# unicode hash
#printf "\x{03B1}\x{03B2}: %s (%uc)\n",$GR,length($GR);
printf "qm:  %s (%uc)\n",$mh58,length($mh58);
our $h58 = $mh58;
# $h58 =~ tr /a-km-zA-HJ-NP-Z/a-km-záéíóúýàèìòùỳâêîôûäëïöüÿ/;
  eval "\$h58 =~ tr /a-km-zA-HJ-NP-Z#/a-km-z${AB}#/;";
printf "h58: %s (%uc)\n",$h58,length($h58);
printf "url63: http://www.%s.localhost:8088/\n",substr($h58,0,63);

}



my $ab = '_0123456789abcdefghijklmnopqrstuvwxyz~'; # 38
#         123456789012345678901234567890123456789
my $gr = decode_utf8 'αβγδεζηθικλμνξοπρσςτυΦχψω'; # 25
my $GR = decode_utf8 'ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ'; # 24
my $FR = decode_utf8 'áéíóúý'.'àèìòùỳ'.'âêîôû'.'äëïöüÿ'.'çœæ'; # 24
my $SP = decode_utf8 'ñõã';
my $DE = decode_utf8 'ÄäÖöÜüẞß';
my $ru = decode_utf8 'ифхЦя';
my $es = decode_utf8 'ĈĜĤĴŜŬĉĝĥĵŝŭ';
my $ph = 'ð'.'';
#         'uɪʏʌɔɐaɑɒɓɗʍwɥʜ';
my $phl = 'pbtdʈɖcɟkɡqɢʔmɱnɳɲŋɴʙrʀⱱɾɽɸβfvθðszʃʒʂʐçʝxɣχʁħʕhɦɬɮʋɹɻjɰlɭʎʟiyɨʉɯ'.
         'uɪʏʊeøɘɵɤoəɛœɜɞʌɔæɐaɶɑɒʘǀǃǂǁɓɗʄɠʛʍwɥʜʢʡɕʑɺɧ'.
#qw(ʼ ʰ ʷ ʲ ˠ ˤ ˡ ).
'|‖'.
'ɚɝɫɬɷɼɿʅʆʇʓʖʗʚʞʠʣʤʥʦʧʨʩʪʫʬʭʮ';
#'ˈˌːˑ ̆|‖‿_͡__̥_̬_'.
#'ăo͡eb̥c̬d̹e̜a̟m̠j̈a̽o̩o̯o˞o̤o̰o̼o̴o̝o̞o̘o̙o̪o̺o̻õoⁿo̚ '.

# '

# lowercase alphabet ...
my $alphab = $ab . $GR . $FR . $SP . $DE . $ru . $es;
printf "AlPhAb: %s (%uc)\n",$alphab,length($alphab);
$alphab = join '', &uniquify(sort (split'',lc($ph.$alphab)));
$alphab =~ tr/_.\n\r\t//ds; # forbidden chars !
printf "\nalphab: %s (%uc)\n",$alphab,length($alphab);
my $base = length($alphab);

my $mha = &encode_basea($mhash,$alphab);
printf "mh%d: %s (%uc)\n",$base,$mha,length($mha);

printf "url64: http://www.%s.localhost:8088/\n",substr($mha,0,63);

my $puny = encode('Punycode',$mha);
printf "puni: %s (%uc)\n",$puny,length($puny);

printf "url: http://www.%s.localhost:8088/\n",$puny;

exit $?;

sub uniquify {
  my %seen = ();
  my @uniq = ();
  foreach (@_) {
    push @uniq,$_ unless $seen{$_}++;
  }
  return @uniq;
}
1;
