---
---
## PunyHash


in order to encode a IPFS CID in a DNS compliant way
we encode the CID with only an alphabet that has 58 characters
and then we bootstingify (punycode) the string to meet the DNS constraints

alphabet : 0123456789abcdefghijklmnopqrstuvwxyzΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ

for instance this document's multihash : QmUgMiZ2miXMaakfgroLSnRHCenqZ6QXQxN8zUQwCi2Vec
                                         qMuGmIz2MIxmAAKFGROlsNrhcENQz6qxqXn8ZuqWcI2vEC

can be translated in with the greek-alphabet into : ΟmΤgΜiΩ2miΧΜaakfgroΛΡnΠΘΓenqΩ6ΟΧΟxΝ8zΤΟwΓi2Υec
                                                    qΜuΗmiz2ΜixmΑΑΚΖΗΠoΛsΝrhcΕΝΟz6qxqΧn8ΩuqΦci2vΕΓ

which is then is encoded to mgi2miaakfgronenq6x8zwi2ec-smri9ktdtae3c4bxaez1b1h4a1c3ct0eva
                            qumiz2ixmosrhcz6qxqn8uqci2v-j7ra2f4bnnyk0itbwg5ce8f7a7zmb8f

which has 59 characters + 4 for the "xn--" header then

  http://xn--qumiz2ixmosrhcz6qxqn8uqci2v-j7ra2f4bnnyk0itbwg5ce8f7a7zmb8f.localhost:8088/
  http://bafybeig54eysrzvozkldgjcglmwdgmglnu6kgmriyxqervzrxywo3jfacy.ipfs.localhost:8080
  
/!\ this leads to the same "coding" density than a base32 format,
so a base36 encoding is more efficient !

a possible subdomain URL can be :

   <https://xn--me5cyu63codi83shvmw3k3ebcsk-zra4b3aavf2a2a2dy4jdahkgdacae.ipfs.dweb.link/>


