#

set -e

IPFS_PATH=$HOME/.../ipfs/usb/PRIVATE
DWNL_PATH=/media/michelc/4TB/Downloads
htmlf="${1:-$DWNL_PATH/Client Area - Freenom.html}"
cp -p "$htmlf" in.html
ls -l "$htmlf"
qm=$(ipfs add -Q --pin=false "in.html")
key=$(perl -S word5.pl $qm | xyml word5)
echo qm: $qm
echo key: $key
# -----------------------------------------------------
# tracking IP
tic=$(date +%s)
if ip=$(curl -s https://postman-echo.com/ip?format=text); then
 echo $tic: $ip 1 $key >> $HOME/.cache/spot/iptraj.log
 age=0
else
 if [ -e $HOME/.cache/spot/iptraj.log ]; then
  line=$(grep -v '0\.0\.' ~/.cache/spot/iptraj.log | tail -1)
  last=$(echo $line | cut -d':' -f1)
  age=$(expr $tic - $last )
  ip=$(echo $line | cut -d' ' -f2)
 else
  ip='0.0.0.0'
  echo $tic: $ip 1 $key >> ~/.cache/spot/iptraj.log
  last=$tic
  age=0
 fi
fi

# -----------------------------------------------------
ipfs add $0 --progress=false >/dev/null
git add $0 >/dev/null
git commit -a -m "commit on $(date) via extractDNS.sh"
# -----------------------------------------------------

echo "htmlf: $htmlf"
echo "key: $key\n";
echo "$tic: $ip; $key"
if [ "$htmlf" != 'clientarea.html' ]; then
cp -p "$htmlf" clientarea.html; git add clientarea.html;
fi
if git commit -uno -m "$tic: dns update for $key" ; then true; fi
cp -p "$htmlf" f-$key.html
pandoc --wrap=none -t markdown -f html "f-$key.html" > f-$key.md
pandoc --wrap=none -t plain -f html "f-$key.html" > f-$key.txt

grep -e token f-$key.html | tee f-$key.log | head -1
grep -i -e '\[Hello ' f-$key.md | tee -a f-$key.log 

mv -f f-fraud.md f-fraud.md~
cat f-$key.md | grep -w 'Fraud' | sed -e 's/  */ /g' >> f-fraud.md~
sort f-fraud.md~ | uniq > f-fraud.md
sed -e 's/[^[]*\[//' -e 's/ *].*//' f-fraud.md > f-skip.txt
cat f-exclude.txt >> f-skip.txt

cat f-$key.md | grep ' Paid ' | sed -e 's/  */ /g' >> f-paid.md
mv f-paid.md f-paid.md~
sort f-paid.md | uniq > f-paid.md

echo -n 'nodes: '
cat f-$key.md | grep ' Free ' | sed -e 's/  */ /g' | tee f-$key.free.md | wc -l

if [ ! -e f-$key.234.yml ]; then
sed -e 's/ | /|/g' -e 's/ ]/]/' -e 's/ \[/- [/' f-$key.free.md | cut -d'|' -f 4,3,2 > f-$key.234.yml
cat *.234.yml | sed -e 's/|/ /g' | grep -v -f f-skip.txt | sort -k 4.1 | uniq > dns.md
fi

sed -e 's/|/ /g' f-$key.234.yml | grep -v -f f-skip.txt | sort -r -k 4.1 | tail -3
fnuser=$(grep -i -e '\[Hello ' f-$key.md | sed -e 's/.*Hello //' -e 's/ ].*//')
email=$(grep -i -e "^$fnuser:" ../accounts.yml| cut -d' ' -f 2)
echo "fnuser: $fnuser <$email>"
echo "$tic: $fnuser <$email>, [$ip] $age" >> ips.yml
sed -e "s/$/|$fnuser|$email/" f-$key.234.yml > f-$key.rp.yml
cat *.rp.yml | sed -e 's/|/ /g' | grep -v -f f-skip.txt | sort -k 4.1 | uniq > rp.md

sed -e 's/|/ /g' f-$key.234.yml | grep -v -f f-skip.txt | sort -r -k 4.1 | tail -1 |\
sed -e "s/\[[^]]*\]/$key: $fnuser <$email> /" -e 's/(\([^)]*\))/[\1](\1)/' > f-$key.next.yml

echo "next maintenance: "
cat *.next.yml | sort -r -k 7.1 | grep -v -f f-banned.txt | uniq | tee next.yml | tail -1

# effets de bord !
cp -p next.yml $HOME/pwiki/_data/next.yml
cp -p dns.md $HOME/pwiki/_includes/dns.md

rm -f *.*~1
qm=$(ipfs add -r -Q --pin=false .)
ipfs name publish --key=fn-next /ipfs/$qm --allow-offline 1>/dev/null &
gwport=$(ipfs config Addresses.Gateway | cut -d'/' -f 5)
echo url: http://127.0.0.1:$gwport/ipfs/$qm/next.yml
tic=$(date +%s)
echo $tic: $qm >> qm.log

last=$(tail -1 next.yml | cut -d' ' -f2 | sed -e 's/://')
rm -i f-$last.next.yml
exit $?
1; # $Source: /my/shell/scripts/extractDNS.sh $
