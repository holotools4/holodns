ZCJ7vxnthaUSCJKxpjeEjZwLouw7rhD4YZFB1qhc8tnubprJL  Pending domains (csvmd)

# DNS:
$TTL=86400
,@,A,86400,104.198.14.52
,cantoute,A,300,212.129.2.151
,www,CNAME,300,website.netlify.app
,surge,CNAME,300,na-west1.surge.sh
,cloud,CNAME,300,cloudflare-ipfs.com
,@,MX,300,10 mx1.improvmx.com
,@,MX,600,15 mx2.improvmx.com
,@,MX,86400,20 mail.mailinator.com
,@,TXT,300,aa-verify=a4baf7bebaffd9eb44624e6bc946ff27328ce300
,_dnslink,TXT,14400,"dnslink=/ipns/QmNzufyrKrzzL3eqNKe6iuDV875SN1Egn2PkKeWxtpNeTk"
,_dnslink,TXT,300,"dnslink=/ipns/QmYwyK9VMxBKm1STUQ9jbYVz3ymAa4ULVLwpx8oHvSKAG2/drt.website.ml"
,_dnslink,TXT,600,"dnslink=/ipns/12D3KooWDbEtrHzozcEbpaPE3gySj8QBib1EpkG4mMrdLLqer81A/www/anonyma.ml"
,_json,TXT,300,'{"qm":"QmXV2wm2uWhCYPsNTUau6whiwCg4hg9CkaVjs9WnVwdLLX"}'
,*,NS,86400,ns1.heliohost.org

# names 
```
zpoint.tk (taken)
zptech.ml
zero-point-health.tk (technical error)

xn--vc-89b.ml
xn--co-qcv.ml
xn--co-grv.ml
rx7
i.agreetm
agreed
lucid.world
worldtm.ml
eden.dreamsm
green.edentm
current-see.ml (taken)
glog.gq
ncov.ga
ncov2.ml
vid19.cf
bootstrap,@,TXT
framasite.tk.,@,A,104.198.14.52
```

## got 0x08823'fied

## got them
zero-point.ml
zero-point-technology.ml
disobedience.gq
frokle.tk,@,TXT,"got it"
docdocgo.ml,@,TXT,"got it (for discovery)"
asksm.ml,@,TXT,"got it"
wealthium.ml,@,TXT,"got it"
gatetm.tk,@,TXT,"\*.180 (got it)"
healthium.ml,@,TXT,"got it"
k171.ml (opendata)
deepledger.ml (ledger startup)
wealthium.tk, "got it"
myceland.*
overlay.ml
credentials.ml,@,TXT,"got it"
pass.xn--libert-gva.ml

## Pending & attempts to create domains
```
economy.ml
skl.ml (skills)
{dead,red,off,out,in}.lines.ml
outage.gq
cahiers.ml (for client's accountancy)
responder™.ml,@,TXT (failed w/ mc@ace 0x08823 (*.102)"
outage.ml,@,TXT,"Error: technical error"
tmpl.gq,@,TXT,"available"
tmpl.tk,@,TXT,"template"
abundance.gq,@,TXT,"Error: technical error (*.138)"
asksm.tk,@,TXT,"taken"
brng.ml.,status,TXT,"available"
buzz.tk, "taken"
creds.ml,@,TXT,"taken"
hellodoc.ml.,status,TXT,"available"
http://itsm.gq
mgcombes.ml.,*,TXT,"Error: technical error"
omics.ml.,status,TXT,"taken"
outage.ml,@,TXT,"We were unable to verify you as a human"
provence.itsm.ml
abundance.gq,@,TXT,"Error: technical error (*.138)"
asksm.tk,@,TXT,"taken"
brng.ml.,status,TXT,"available"
buzz.tk, "taken"
credentials.ml,@,TXT,"We were unable to verify you as a human"
creds.ml,@,TXT,"taken"
hellodoc.ml.,status,TXT,"available"
http://itsm.gq
mgcombes.ml.,*,TXT,"Error: technical error"
omics.ml.,status,TXT,"taken"
outage.ml,@,TXT,"Error: technical error"
outage.ml,@,TXT,"We were unable to verify you as a human"
provence.itsm.ml
```

# old site that broke...
```
COSMOSTV.ML,contact,RP,"evolution@gc-bank.org. ."
mutable.gq.,contact,RP,"michelcombes@advancementofcivilizationeffort.org. ."
MUTABLES.TK.,contact,RP,"michelcombes@advancementofcivilizationeffort.org. ."
ONE--YDA2C72A.TK.,contact,RP,"michelcombes@advancementofcivilizationeffort.org. ."
signedtm.tk.,contact,RP,"mgcombes@gmail.com. ."
TOXIK.ML.,contact,RP,"michelcombes@advancementofcivilizationeffort.org. ."
WEATHER-ON-DEMAND.GQ.,contact,RP,"mgcombes@gmail.com. ."
```


## canceled w/ mcombes
- marketsm.ml: https://my.freenom.com/clientarea.php?action=domaindetails&id=1074466354
- mgctm.ml: https://my.freenom.com/clientarea.php?action=domaindetails&id=1074468082
- loginsm.tk: https://my.freenom.com/clientarea.php?action=domaindetails&id=1074468086
- jsonweb.tk: https://my.freenom.com/clientarea.php?action=domaindetails&id=1074468088
- omix.gq: https://my.freenom.com/clientarea.php?action=domaindetails&id=1074467360
- nutrium.ml  Active  1 Days Ago  Past Renewable Period   

### missed !

- cerestm.tk -> cerestm.ml instead
- omicsm.tk Active  1 Days Ago  Past Renewable Period   
- omicsm.ml Active  1 Days Ago  Past Renewable Period   
- weather-on-demand.gq  Active  1 Days Ago  Past Renewable Period

### dropped 
```
iggyl@sniptext.com:@2gether.international.,*,TXT,2020.07.31
evolution@gc-bank.org:@cnnp.ml,*,TXT,2021-04-02
evolution@gc-bank.org:@ridicule.gq,*,TXT,2021-04-02
```

### catch-all sites

# xn--x78h.gq
# note catchall site redirect :
- xn--b9g.ml (m-combes@gc-bank.org)
- xn--xgj.ml (ⱺ.ml from union@gc-banktm.com)

## flagged
```
loginsm.tk,@,TXT,"flagged abused: cancelled"
otltm.tk,@,TXT,"flagged abused: cancelled"
```


### lost from timebank@gc-bank.org
```
- [xn\--047h.gq](http://xn--047h.gq/) 2018-11-12 2021-11-12 Fred timebank@gc-bank.org
- [xn\--047h.ml](http://xn--047h.ml/) 2018-11-12 2021-11-12 Fred timebank@gc-bank.org
- [xn\--0m8h7u.tk](http://xn--0m8h7u.tk/) 2018-11-12 2021-11-12 Fred timebank@gc-bank.org
- [xn\--147h.ga](http://xn--147h.ga/) 2018-11-12 2021-11-12 Fred timebank@gc-bank.org
- [xn\--du8h.cf](http://xn--du8h.cf/) 2018-11-12 2021-11-12 Fred timebank@gc-bank.org
- [xn\--e18h.ml](http://xn--e18h.ml/) 2018-11-12 2021-11-12 Fred timebank@gc-bank.org
- [xn\--l7g.ml](http://xn--l7g.ml/) 2018-11-12 2021-11-12 Fred timebank@gc-bank.org
- [xn\--l7g.tk](http://xn--l7g.tk/) 2018-11-12 2021-11-12 Fred timebank@gc-bank.org
- [xn\--lq8h.ml](http://xn--lq8h.ml/) 2018-11-12 2021-11-12 Fred timebank@gc-bank.org
- [xn\--lv8h.gq](http://xn--lv8h.gq/) 2018-11-12 2021-11-12 Fred timebank@gc-bank.org
- [xn\--nt8h.ga](http://xn--nt8h.ga/) 2018-11-12 2021-11-12 Fred timebank@gc-bank.org
- [xn\--u7h.cf](http://xn--u7h.cf/) 2018-11-12 2021-11-12 Fred timebank@gc-bank.org
- [timebank.ml](http://timebank.ml/) 2019-07-25 2022-07-25 MichelC m69.iph.heliohost.org
- [xn\--047h.gq](http://xn--047h.gq/) 2018-11-12 2022-11-12 Fred timebank@gc-bank.org
- [xn\--047h.ml](http://xn--047h.ml/) 2018-11-12 2022-11-12 Fred timebank@gc-bank.org
- [xn\--0m8h7u.tk](http://xn--0m8h7u.tk/) 2018-11-12 2022-11-12 Fred timebank@gc-bank.org
- [xn\--147h.ga](http://xn--147h.ga/) 2018-11-12 2022-11-12 Fred timebank@gc-bank.org
- [xn\--du8h.cf](http://xn--du8h.cf/) 2018-11-12 2022-11-12 Fred timebank@gc-bank.org
- [xn\--e18h.ml](http://xn--e18h.ml/) 2018-11-12 2022-11-12 Fred timebank@gc-bank.org
- [xn\--l7g.ml](http://xn--l7g.ml/) 2018-11-12 2022-11-12 Fred timebank@gc-bank.org
- [xn\--l7g.tk](http://xn--l7g.tk/) 2018-11-12 2022-11-12 Fred timebank@gc-bank.org
- [xn\--lq8h.ml](http://xn--lq8h.ml/) 2018-11-12 2022-11-12 Fred timebank@gc-bank.org
- [xn\--lv8h.gq](http://xn--lv8h.gq/) 2018-11-12 2022-11-12 Fred timebank@gc-bank.org
- [xn\--nt8h.ga](http://xn--nt8h.ga/) 2018-11-12 2022-11-12 Fred timebank@gc-bank.org
- [xn\--u7h.cf](http://xn--u7h.cf/) 2018-11-12 2022-11-12 Fred timebank@gc-bank.org
- [timebank.gq](http://timebank.gq/) 2022-07-16 2023-07-16 Iggy iggyl@sniptext.com
- [timebank.ml](http://timebank.ml/) 2019-07-25 2023-07-25 MichelC m69.iph.heliohost.org

```

### lost from m.combes@gc-bank.org
```
atsm.tk,_expires,TXT,"2020-11-15"
biobot.tk,_expires,TXT,"2020-11-15"
gc-bank.tk,_expires,TXT,"2020-03-27"
gcoin.mk,_expires,TXT,"2020-02-14"
sunride.tk,_expires,TXT,"2020-02-14"
unspam.tk,_expires,TXT,"2020-02-17"
```
### load from union@gc-banktm.com
```
marketsm.tk   2019-09-27  2020-09-27  Fraud
```
### lost from michel@iph.heliohost.org
```
in-or-out.tk.,_expires,TXT,"24.06.2020"
inorout.tk.,_expires,TXT,"24.06.2020"
justicesm.ml,_expires,"2020-06-24"
logsm.ml,_expires,TXT,"2020-06-24"
safetysm.ml.,_expires,TXT,"24.06.2020"
trustedtm.tk.,_expires,TXT,"24.06.2020"
distributed.gq.,_expires,TXT,"18.03.2020"
dhsm.tk.,_expires,TXT,"18.03.2020"

```
### lost from michelcombes@advancementofcivilizationeffort.org
```
adlt.tk.,_expires,TXT,"20.10.2019"
blockrings.tk.,contact,RP,"bring.sniptext.com. ."
blockrings.tk.,_expires,TXT,"15.08.2020"
iphs.ga.,_expires,TXT,"26.09.2019"
iplt.tk.,_expires,TXT,"22.10.2019"
#mutable.gq.,_expires,TXT,"21.10.2019"
qrco.gq,*,TXT,"cancelled"
sayitlouder.tk.,_expires,TXT,"19.10.2019"
shared.gq.,_expires,TXT,"05.10.2019; now porn !"
toxiq.tk.,_expires,TXT,"19.10.2019"
united-earth.ga,_expires.TXT,"19.08.2019"
worldy.gq.,_expires,TXT,"19.10.2019"
worldy.ml.,_expires,TXT,"19.10.2019"
xn--nw8h.tk.,_expires,TXT,"04.10.2019"
xn--ug8h.gq.,_expires,TXT,"04.09.2019"

citoyenne.gq.,_expires,TXT,"01.06.2022"
citoyenne.ml.,_expires,TXT,"01.06.2022"
reference.ml.,_expires,TXT,"01.06.2022"
voter.gq.,_expires,TXT,"01.06.2022"
xn--assemble-h1a.ga,_expires,TXT,"01.06.2022"
xn--evolution-fna.ga,_expires,TXT,"01.06.2022"
xn--j77hya.ga,_expires,TXT,"01.06.2022"
xn--j77hya.gq,_expires,TXT,"01.06.2022"
xn--j77hya.tk,_expires,TXT,"01.06.2022"
```
### lost from gradual.moves@gmail.com
```
bybtm.gq,_expires,TXT,"03.12.2019"
bybtm.ml,_expires,TXT,"03.12.2019"
bybtm.tk,_expires,TXT,"03.12.2019"
gradual-quanta.gq,_expires,TXT,"03.12.2019"
gradual-quanta.tk,_expires,TXT,"03.12.2019"
uncsm.gq,_expires,TXT,"03.12.2019"
wordstm.gq,_expires,TXT,"03.12.2019"
```
### lost from essenctango@gmail.com
```
essencetango.cf,_expired,TXT,"11.11.2019"
essencetango.ga,@,TXT,"flagged abused: cancelled"
essencetango.gq,@,TXT,"flagged abused: cancelled"
essencetango.tk,_expired,TXT,"06.11.2019"
xn--147h.ml|🆔.ml,_expired,TXT,"11.11.2019 taken ?"
xn--147h.gq|🆔.gq,_expired,TXT,"11.11.2019 taken"
mgctm.gq,@,TXT,"flagged abused: cancelled"
RXTHM.ML,_expired,TXT,"14.09.2019"
xn--ir8hop.cf,_expired,TXT,"06.11.2019"
```
### lost from mgcombes@gmail.com
```
iuris.gq,_expired,TXT,"10.02.2020"
mutables.tk,_expired,TXT,"10.02.2020"
workmeeter.ml,_expired,TXT,"10.02.2020"
weather-on-demand.gq,_expired,TXT,10/02/2020
pending.cf,_expired,TXT,10/02/2020
provable.tk,_expired,TXT, 10/02/2020
projectstm.gq,_expired,TXT, 10/02/2020
projectstm.tk,_expired,TXT, 10/02/2020
le-vrai-debat.ml,_expired,TXT,10/02/2020
smart-litigation.ml,_expired,TXT, 10/02/2020
signedtm.tk,_expired,TXT, 10/02/2020 (got https://signed.tk)
u-nion.info,_expired,TXT, 22/07/2020
```

### lost from jhibma@hash.fyi (Jarrod)

```
exquis.ml,_expired,TXT,"10.05.2022"
```
