## holoDNS


Remove the centralization of DNS by using a local DNS daemon running locally
to cache and "VALIDATE" the DNS record according to a consensus on the holoRing (blockRing™)

1. first you need static IPAddress
2. second you need to run [[dnsmasq]]
   - for installation see (tabs: <https://www.one-tab.com/page/4JazcZdPTIWkgaJF3ZJAHQ>)
3. configure the [[holoServe]] to update your Zone files


covered domains :
 https://holotools4.frama.io/holodns/mydomain.htm


